﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Mrz_Lab_1.Models
{
  public sealed class LabMainModel
  {
    public LabMainModel()
    {
      this.Parts = new List<Bitmap>();
      this.OutputParts = new List<Bitmap>();
    }

    public ResultModel ResultModel { get; set; }

    public Bitmap SourceImage { get; set; }

    public string ImagePath { get; set; }

    public int PartWidth { get; set; }

    public int PartHeight { get; set; }

    public int FullWidth { get; set; }

    public int FullHeight { get; set; }

    public IList<Bitmap> Parts { get; set; }

    public IList<Bitmap> OutputParts;

    public int SecondLayerNeuronsCount { get; set; }

    public int MaxError { get; set; }

    public double LearningRate { get; set; }

    public int EpochCount { get; set; }

    public string ImageName { get; set; }
  }
}
