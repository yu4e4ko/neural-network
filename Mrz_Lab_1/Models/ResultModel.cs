﻿using System.Drawing;

namespace Mrz_Lab_1.Models
{
  public class ResultModel
  {
    public double Error { get; set; }

    public int EpochCount { get; set; }

    public Bitmap OutputImage { get; set; }

    public double ZFactor { get; set; }
  }
}
