﻿using MathNet.Numerics.LinearAlgebra;
using Mrz_Lab_1.Models;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Mrz_Lab_1.ImageUtils
{
  internal sealed class ImageHelper
  {
    public List<Bitmap> SplitSourceImage(LabMainModel labMainModel)
    {
      var parts = new List<Bitmap>();

      var sourceBitmap = labMainModel.SourceImage;

      var sourceHeight = labMainModel.FullHeight;
      var sourceWidth = labMainModel.FullWidth;

      var partWidth = labMainModel.PartWidth > sourceWidth ? sourceWidth : labMainModel.PartWidth;
      var partHeight = labMainModel.PartHeight > sourceHeight ? sourceHeight : labMainModel.PartHeight;

      // Количество прямоугольников по длине
      var widthCount = Math.Ceiling(sourceWidth / (double)labMainModel.PartWidth);

      // Количество прямоугольников по ширине
      var heightCount = Math.Ceiling(sourceHeight / (double)labMainModel.PartHeight);

      var totalRects = widthCount * heightCount;

      var x = 0;
      var y = 0;
      var horizontRect = 0;
      var verticalRect = 0;

      for (var rect = 0; rect < totalRects; rect++)
      {
        if(horizontRect >= widthCount)
        {
          horizontRect = 0;
          verticalRect++;
        }

        var bitmapRectangle = new Bitmap(partWidth, partHeight);

        for(var i = 0; i < partHeight; i++)
        {
          for(var j = 0; j < partWidth; j++)
          {
            x = horizontRect * partWidth + j;
            y = verticalRect * partHeight + i;

            if (x < sourceWidth && y < sourceHeight)
            {
              bitmapRectangle.SetPixel(j, i, sourceBitmap.GetPixel(x, y));
            }
            else
            {
              bitmapRectangle.SetPixel(j, i, Color.White);
            }
          }
        }

        parts.Add(bitmapRectangle);

        horizontRect++;
      }

      return parts;
    }

    public static List<double> GetColors(Bitmap bitmap, string colorName)
    {
      var colors = new List<double>();

      int h = bitmap.Height;
      int w = bitmap.Width;
      Color color = new Color();

      for (int i = 0; i < h; i++)
      {
        for (int j = 0; j < w; j++)
        {
          color = bitmap.GetPixel(j, i);
          switch (colorName)
          {
            case "RED":
              colors.Add(Normalize(color.R));
              break;
            case "GREEN":
              colors.Add(Normalize(color.G));
              break;
            case "BLUE":
              colors.Add(Normalize(color.B));
              break;
          }
        }
      }

      return colors;
    }

    public static Bitmap ReconstrustImage(int index, Bitmap inputImage, List<Matrix<double>> X2)
    {
      int n = inputImage.Height;
      int m = inputImage.Width;
      Bitmap image = new Bitmap(m, n);
      int N = n * m * 3;
      byte[] reconstructPix = new byte[3];
      int pixel = 0;
      Color[] color = new Color[N];

      for (int i = 0; i < n * m; i++)
      {
        reconstructPix[0] = ReconstructPixel(X2[index][0, i]);
        reconstructPix[1] = ReconstructPixel(X2[index][0, i + n * m]);
        reconstructPix[2] = ReconstructPixel(X2[index][0, i + 2 * n * m]);
        color[pixel++] = Color.FromArgb(reconstructPix[0], reconstructPix[1], reconstructPix[2]);
      }

      pixel = 0;

      for (int i = 0; i < n; i++)
      {
        for (int j = 0; j < m; j++)
        {
          image.SetPixel(j, i, color[pixel]);
          pixel++;
        }
      }
      return image;
    }

    public static Bitmap CreateImageFromParts(LabMainModel labMainModel)
    {
      var x = 0;
      var y = 0;

      var wCount = (int)Math.Ceiling(labMainModel.FullWidth / (double)labMainModel.PartWidth);
      var hCount = (int)Math.Ceiling(labMainModel.FullHeight / (double)labMainModel.PartHeight);

      var w = wCount * labMainModel.PartWidth;
      var h = hCount * labMainModel.PartHeight;

      var images = labMainModel.Parts;

      var outputImage = new Bitmap(w, h);

      foreach (Bitmap image in labMainModel.OutputParts)
      {
        for (int i = 0; i < image.Width; i++)
        {
          for (int j = 0; j < image.Height; j++)
          {
            outputImage.SetPixel(x + i, y + j, image.GetPixel(i, j));
          }
        }

        if (x + labMainModel.PartWidth < w)
        {
          x += labMainModel.PartWidth;
        }
        else
        {
          x = 0;

          if (y + labMainModel.PartHeight < h)
          {
            y += labMainModel.PartHeight;
          }
        }

      }

      return outputImage;
    }

    private static double Normalize(int colorValue)
    {
      var result = (2.0 * colorValue / 255) - 1.0;
      return result;
    }

    public static byte ReconstructPixel(double x)
    {
      x = x < -1 ? -1 : x;
      x = x > 1 ? 1 : x;

      var result = (byte)((255 * (x + 1)) / 2);
      return result;
    }

  }
}
