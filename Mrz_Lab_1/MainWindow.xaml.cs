﻿using Microsoft.Win32;
using Mrz_Lab_1.ImageUtils;
using Mrz_Lab_1.Models;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Mrz_Lab_1
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private LabMainModel _labMainModel;
    private int _currentPart = -1;

    //todo: should be set by user
    private const int SecondLayerNeuronsCount = 30;
    private const int MaxError = 130;
    private const double LearningRate = 0.005;
    private const int EpochCount = 1000;

    public MainWindow()
    {
      InitializeComponent();
      this.Title = "MRZ. Lab1. Yuri Chupyrkin";
      this.ResizeMode = ResizeMode.CanMinimize;

      AllocConsole();
      this.Init();
    }

    #region eventes

    #region console output

    /// <summary>
    /// Allocates a new console for current process.
    /// </summary>
    [DllImport("kernel32.dll")]
    public static extern Boolean AllocConsole();

    /// <summary>
    /// Frees the console.
    /// </summary>
    [DllImport("kernel32.dll")]
    public static extern Boolean FreeConsole();

    #endregion

    private void LoadConfigMenu_Click(object sender, RoutedEventArgs e)
    {
      var openFileDialog = new OpenFileDialog();
      openFileDialog.Filter = "Bin|*.bin";

      if (openFileDialog.ShowDialog() == true)
      {
        var fileInfo = new FileInfo(openFileDialog.FileName);
        var uri = new Uri(openFileDialog.FileName);

        if (this.ValidateModel())
        {
          //todo: temp set
          _labMainModel.SecondLayerNeuronsCount = SecondLayerNeuronsCount;
          _labMainModel.MaxError = MaxError;
          _labMainModel.LearningRate = LearningRate;
          _labMainModel.EpochCount = EpochCount;

          try
          {
            this.LoadAndRun(uri);
          }
          catch (Exception ex)
          {
            MessageBox.Show(ex.Message, "ERROR");
          }
        }
      }
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      FreeConsole();
    }

    private void ExitMenu_Click(object sender, RoutedEventArgs e)
    {
      Environment.Exit(1);
    }

    private void NewMenu_Click(object sender, RoutedEventArgs e)
    {
      this.Init();
    }

    private void btnOpenNewImage_Click(object sender, RoutedEventArgs e)
    {
      var openFileDialog = new OpenFileDialog();
      openFileDialog.Filter = "Images|*.bmp;*.jpg;*.png;*.gif;*.tif";

      if (openFileDialog.ShowDialog() == true)
      {
        var fileInfo = new FileInfo(openFileDialog.FileName);
        var uri = new Uri(openFileDialog.FileName);
        this.SetSourceImageInfo(uri);
      }
    }

    private void btnOpenSourceImage_Click(object sender, RoutedEventArgs e)
    {
      try {
        var processStartInfo = new ProcessStartInfo();
        processStartInfo.FileName = _labMainModel.ImagePath;
        Process.Start(processStartInfo);
      }
      catch(Exception ex)
      {
        MessageBox.Show(string.Format("Error! {0}", ex.Message));
      }
    }

    private void txtWidth_LostFocus(object sender, RoutedEventArgs e)
    {
      var width = txtWidth.Text;
      int widthValue;

      if (int.TryParse(width, out widthValue))
      {
        _labMainModel.PartWidth = widthValue;
      }
      else
      {
        txtWidth.Text = "0";
        _labMainModel.PartWidth = 0;
      }
    }

    private void txtHeight_LostFocus(object sender, RoutedEventArgs e)
    {
      var height = txtHeight.Text;
      int heightValue;

      if (int.TryParse(height, out heightValue))
      {
        _labMainModel.PartHeight = heightValue;
      }
      else
      {
        txtHeight.Text = "0";
        _labMainModel.PartHeight = 0;
      }
    }

    private void btnRun_Click(object sender, RoutedEventArgs e)
    {
      if (this.ValidateModel())
      {
        //todo: temp set
        _labMainModel.SecondLayerNeuronsCount = SecondLayerNeuronsCount;
        _labMainModel.MaxError = MaxError;
        _labMainModel.LearningRate = LearningRate;
        _labMainModel.EpochCount = EpochCount;

        try {
          this.Run();
        }
        catch(Exception ex)
        {
          MessageBox.Show(ex.Message, "ERROR");
        }
      }
    }

    private void btnNextPart_Click(object sender, RoutedEventArgs e)
    {
      if(this.ModelIsNotNullAndHasParts() == false)
      {
        return;
      }

      if (_labMainModel.Parts.Count > _currentPart + 1)
      {     
        _currentPart++;
      }

      imagePart.Source = this.BitmapToBitmapImage(_labMainModel.Parts[_currentPart]);
      labelCurrentPart.Content = (_currentPart + 1).ToString();
    }

    private void btnPrevPart_Click(object sender, RoutedEventArgs e)
    {
      if (this.ModelIsNotNullAndHasParts() == false)
      {
        return;
      }

      if (_currentPart > 0)
      {    
        _currentPart--;
      }

      imagePart.Source = this.BitmapToBitmapImage(_labMainModel.Parts[_currentPart]);
      labelCurrentPart.Content = (_currentPart + 1).ToString();
    }

    #endregion

    private bool ModelIsNotNullAndHasParts()
    {
      if (_labMainModel == null)
      {
        return false;
      }

      if (_labMainModel.Parts == null)
      {
        return false;
      }

      return true;
    }

    private BitmapSource BitmapToBitmapImage(Bitmap bitmap)
    {
      var hBitmap = bitmap.GetHbitmap();
      var result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
        hBitmap, 
        IntPtr.Zero, 
        Int32Rect.Empty, 
        BitmapSizeOptions.FromEmptyOptions());

      return result;
    }

    private void Init()
    {
      _labMainModel = new LabMainModel();
      _currentPart = -1;

      this.SetControlsState(true);
    }

    private void SetSourceImageInfo(Uri uri)
    {
      BitmapImage image = new BitmapImage(uri);
      imgSource.Source = image;
      txtSourceImagePath.Text = uri.ToString();

      _labMainModel.ImagePath = uri.ToString();

      _labMainModel.SourceImage = this.BitmapImageToBitmap(image);

      if (uri.IsFile)
      {
        string filename = Path.GetFileName(uri.LocalPath);
        _labMainModel.ImageName = filename;
      }   

      _labMainModel.FullWidth = (int)(_labMainModel.SourceImage.Width);
      _labMainModel.FullHeight = (int)(_labMainModel.SourceImage.Height);

      this.SetControlsState(false);
    }

    private void SetControlsState(bool isNewSession)
    {
      btnOpenNewImage.IsEnabled = isNewSession;
      btnOpenSourceImage.IsEnabled = !isNewSession;
      btnRun.IsEnabled = !isNewSession;

      if (isNewSession)
      {
        imgSource.Source = null;
        imagePart.Source = null;
        txtSourceImagePath.Text = string.Empty;
        txtHeight.Text = "0";
        txtWidth.Text = "0";
      }
    }

    private bool ValidateModel()
    {
      if(_labMainModel == null)
      {
        MessageBox.Show("Error! Model is null");
        return false;
      }

      if (_labMainModel.FullHeight == 0 || _labMainModel.FullWidth == 0 || string.IsNullOrEmpty(_labMainModel.ImagePath))
      {
        MessageBox.Show("Error! Incorrect source image");
        return false;
      }

      if (_labMainModel.PartHeight < 1 || _labMainModel.PartWidth < 1)
      {
        MessageBox.Show("Error! Incorrect height or width");
        return false;
      }

      if (_labMainModel.PartHeight > _labMainModel.FullHeight)
      {
        _labMainModel.PartHeight = _labMainModel.FullHeight;
        txtHeight.Text = _labMainModel.FullHeight.ToString();
      }

      if (_labMainModel.PartWidth > _labMainModel.FullWidth)
      {
        _labMainModel.PartWidth = _labMainModel.FullWidth;
        txtWidth.Text = _labMainModel.FullWidth.ToString();
      }

      return true;
    }

    private void PrepareParts()
    {
      var imageHelper = new ImageHelper();
      _labMainModel.Parts = imageHelper.SplitSourceImage(_labMainModel);
      labelPartsCount.Content = _labMainModel.Parts.Count;
      imagePart.Source = null;
      _currentPart = -1;

      MessageBox.Show(string.Format("Image contains {0} parts\nPress Ok for next step", _labMainModel.Parts.Count));
    }

    private void Run()
    {
      this.PrepareParts();

      //Todo: start..
      var neuralNetwork = new NeuralNetwork(_labMainModel);
      var labModel = neuralNetwork.Train();

      MessageBox.Show("Epoch: " + labModel.ResultModel.EpochCount);
      MessageBox.Show("Error: " + labModel.ResultModel.Error);
      MessageBox.Show("Z factor: " + labModel.ResultModel.ZFactor);

      SaveBitmapAndOpen(labModel.ResultModel.OutputImage);
    }

    private void LoadAndRun(Uri uri)
    {
      MessageBox.Show("Feature is obsoleted");
    }

    private void SaveBitmapAndOpen(Bitmap bitmap)
    {
      if(bitmap == null)
      {
        return;
      }

      try
      {
        var fileName = DateTime.Now.Ticks.ToString() + ".png";
        bitmap.Save(fileName, ImageFormat.Png);

        var processStartInfo = new ProcessStartInfo();
        processStartInfo.FileName = fileName;
        Process.Start(processStartInfo);
      }
      catch (Exception ex)
      {
        MessageBox.Show(string.Format("Error! {0}", ex.Message));
      }
    }

    private Bitmap BitmapImageToBitmap(BitmapImage bitmapImage)
    {
      Bitmap result;

      using (var memoryStream = new MemoryStream())
      {
        var enc = new BmpBitmapEncoder();
        enc.Frames.Add(BitmapFrame.Create(bitmapImage));
        enc.Save(memoryStream);
        result = new Bitmap(memoryStream);
      }

      return result;
    }
  }
}
