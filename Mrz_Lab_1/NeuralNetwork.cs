﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Mrz_Lab_1.ImageUtils;
using Mrz_Lab_1.Models;
using System;
using System.Collections.Generic;

namespace Mrz_Lab_1
{
  public sealed class NeuralNetwork 
  {
    private LabMainModel _labMainModel;

    private readonly int _epochCount;
    private readonly double _learningRate;
    private readonly int _maxError;
    private readonly int _secondLayerNeuronsCount;
    private readonly int _firstLayerNeuronsCount;
    private readonly int _partHeight;
    private readonly int _partWidth;
    private readonly int _imagesCount;

    private int _currentEpoch;
    private double _error;

    private Matrix<double> _w2;
    private Matrix<double> _w1;

    private List<Matrix<double>> _x;
    private List<Matrix<double>> _y;
    private List<Matrix<double>> _dX;

    public NeuralNetwork()
    {

    }

    public NeuralNetwork(LabMainModel labMainModel)
    {
      if (labMainModel == null)
      {
        throw new ArgumentException("labMainModel is null (NeuralNetwork constructor)");
      }

      _labMainModel = labMainModel;

      _learningRate = _labMainModel.LearningRate;
      _epochCount = _labMainModel.EpochCount;
      _maxError = _labMainModel.MaxError;
      _secondLayerNeuronsCount = _labMainModel.SecondLayerNeuronsCount;

      _partHeight = _labMainModel.PartHeight;
      _partWidth = _labMainModel.PartWidth;
      _imagesCount = _labMainModel.Parts.Count;

      _firstLayerNeuronsCount = _partWidth * _partHeight * 3;
      _error = Double.MaxValue;
      _currentEpoch = 0;

      this.Init();
    }

    public LabMainModel Train()
    {
      this.ConsoleLog();

      while (_error > _maxError && _currentEpoch < _epochCount)
      {
        _currentEpoch++;

        for (int j = 0; j < _imagesCount; j++)
        {
          _y[j] = _x[j].Multiply(_w1);
          _dX[j] = _y[j].Multiply(_w2).Subtract(_x[j]);

          this.AdjustWeights(j);
        }

        _error = this.CalculateError();

        if (_error.Equals(Double.NaN))
        {
          _error = double.MaxValue;
        }

        this.ConsoleLog();
      }

      List<Matrix<Double>> decompressed = new List<Matrix<Double>>();

      for (int i = 0; i < _imagesCount; i++)
      {
        _y[i] = _x[i].Multiply(_w1);
        decompressed.Add(_y[i].Multiply(_w2));
        _labMainModel.OutputParts.Add(ImageHelper.ReconstrustImage(i, _labMainModel.Parts[i], decompressed));
      }

      var outputImage = ImageHelper.CreateImageFromParts(_labMainModel);

      var resultModel = new ResultModel
      {
        Error = _error,
        EpochCount = _currentEpoch,
        OutputImage = outputImage,
        ZFactor = this.BuildZFactor()
      };

      _labMainModel.ResultModel = resultModel;

      Console.WriteLine("Complete!\n");
      ConsoleShowWeights("W1", _w1);
      ConsoleShowWeights("W2", _w2);

      return _labMainModel;
    }

    #region privates

    public double BuildZFactor()
    {
      return ((_firstLayerNeuronsCount + _imagesCount) * _secondLayerNeuronsCount + 2.0) / (_firstLayerNeuronsCount * _imagesCount);
    }

    private double CalculateError()
    {
      double error = 0;

      for (int i = 0; i < _imagesCount; i++)
      {
        _y[i] = _x[i].Multiply(_w1);
        _dX[i] = _y[i].Multiply(_w2).Subtract(_x[i]);

        for (int j = 0; j < _firstLayerNeuronsCount; j++)
        {
          error += _dX[i][0, j] * _dX[i][0, j];
        }
      }

      return error / 2.0;
    }

    private void AdjustWeights(int i)
    {
      var xAdaptStep = this.CalculateAdaptStep(_x, i);
      _w1 = _w1.Subtract(_x[i].Transpose().Multiply(_dX[i]).Multiply(_w2.Transpose()).Multiply(xAdaptStep));

      var yAdaptStep = this.CalculateAdaptStep(_y, i);
      _w2 = _w2.Subtract(_y[i].Transpose().Multiply(_dX[i]).Multiply(yAdaptStep));

      this.AdjustWeightsWithNormalize();
    }

    private void AdjustWeightsWithNormalize()
    {
      _w1 = this.NormalizeWeights(_w1);
      _w2 = this.NormalizeWeights(_w2);
    }

    private Matrix<double> NormalizeWeights(Matrix<double> w)
    {
      for (var j = 0; j < w.ColumnCount; j++)
      {
        double norm = 0;
        for (var i = 0; i < w.RowCount; i++)
        {
          norm += w[i, j] * w[i, j];
        }
        var result = Math.Sqrt(norm);

        for (var i = 0; i < w.RowCount; i++)
        {
          w[i, j] = w[i, j] / result;
        }
      }

      return w;
    }

    private double CalculateAdaptStep(List<Matrix<double>> x, int i)
    {
      double totalSum = 1;

      for (var j = 0; j < x[i].ColumnCount; j++)
      {
        totalSum += x[i][0, j];
      }

      var adaptStep = 1 / Math.Pow(totalSum, 2);

      return (adaptStep < 0.001) ? 0.001 : (adaptStep > 0.005) ? 0.005 : adaptStep;
    }

    private void Init()
    {
      _w1 = this.CreateRandomMatrix(_firstLayerNeuronsCount, _secondLayerNeuronsCount);
      _w2 = _w1.Transpose();

      _x = this.CreateLayers(_firstLayerNeuronsCount);
      _dX = this.CreateLayers(_secondLayerNeuronsCount);
      _y = this.CreateLayers(_secondLayerNeuronsCount);

      this.SetValuesForInputLayer();
    }

    private void SetValuesForInputLayer()
    {
      var reds = new List<double>();
      var greens = new List<double>();
      var blues = new List<double>();

      var iterationCount = _partWidth * _partHeight;

      for (int i = 0; i < _imagesCount; i++)
      {
        reds = ImageHelper.GetColors(_labMainModel.Parts[i], "RED");
        greens = ImageHelper.GetColors(_labMainModel.Parts[i], "GREEN");
        blues = ImageHelper.GetColors(_labMainModel.Parts[i], "BLUE");

        if (reds.Count < iterationCount || greens.Count < iterationCount || blues.Count < iterationCount)
        {
          throw new InvalidOperationException("Colors less then iterations");
        }

        for (int j = 0; j < iterationCount; j++)
        {
          _x[i][0, j] = reds[j];
          _x[i][0, j + iterationCount] = greens[j];
          _x[i][0, j + 2 * iterationCount] = blues[j];
        }
      }
    }

    private List<Matrix<double>> CreateLayers(int size)
    {
      var layer = new List<Matrix<double>>();
      for (var i = 0; i < _imagesCount; i++)
      {
        layer.Add(new DenseMatrix(1, size));
      }

      return layer;
    }

    private Matrix<double> CreateRandomMatrix(int firstLayerNeuronsCount, int secondLayerNeuronsCount)
    {
      var matrix = new DenseMatrix(firstLayerNeuronsCount, secondLayerNeuronsCount);

      Random rand = new Random(1);
      for (int i = 0; i < matrix.RowCount; i++)
      {
        for (int j = 0; j < matrix.ColumnCount; j++)
        {
          matrix[i, j] = 0.1 * rand.NextDouble() + 0.1;
        }
      }

      return matrix;
    }

    private void ConsoleLog()
    {
      var message = string.Format("__________\n"
        + "Epoch: {0}\n"
        + "Error: {1}\n"
        + "Max epoch: {2}\n"
        + "Min error: {3}\n"
        + "__________\n\n\n",
        _currentEpoch,
        _error,
        _epochCount,
        _maxError);

      Console.WriteLine(message);
    }

    private void ConsoleShowWeights(string name, Matrix<double> w)
    {
      var result = string.Empty;
      for(var i = 0; i < w.RowCount; i++)
      {
        for(var j = 0; j < w.ColumnCount; j++)
        {
          result += string.Format("[{0}]", w[i,j].ToString("#.####"));
        }

        result += Environment.NewLine;
      }

      Console.WriteLine("{0}------------------------------", Environment.NewLine);
      Console.WriteLine("Matrix: {0}", name);
      Console.WriteLine(result);
      Console.WriteLine("{0}------------------------------", Environment.NewLine);
    }

    #endregion
  }
}
